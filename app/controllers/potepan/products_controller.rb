class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.includes(:product_properties).find(params[:id])
  end
end
