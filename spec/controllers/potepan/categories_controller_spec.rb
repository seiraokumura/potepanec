require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET #show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { taxonomy.root }
    let(:taxon_child) { create(:taxon, taxonomy: taxonomy, parent: taxon) }
    let(:product1) { create(:product, name: "correct product", taxons: [taxon_child]) }
    let(:product2) { create(:product, name: "incorrect product") }

    before { get :show, params: { id: taxon_child.id } }

    it 'has a 200 status code' do
      expect(response).to have_http_status(:ok)
    end

    it 'assigns @taxon' do
      expect(assigns(:taxon)).to eq taxon_child
    end

    it 'assigns @taxonomies' do
      expect(assigns(:taxonomies)).to match_array(taxonomy)
    end

    it 'assigns correct @products' do
      expect(assigns(:products)).to match_array(product1)
    end

    it 'assigns incorrect @products' do
      expect(assigns(:products)).not_to include(product2)
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end
  end
end
